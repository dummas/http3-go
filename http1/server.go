package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"path"
	"strings"
)

func main() {
	l, _ := net.Listen("tcp", "localhost:3333")
	defer l.Close()
	log.Printf("Listening on %s", l.Addr())

	for {
		conn, _ := l.Accept()
		handleRequest(conn)
		conn.Close()
	}
}

func handleRequest(conn net.Conn) {
	buf := make([]byte, 1024)
	conn.Read(buf)
	lines := strings.Split(string(buf), "\r\n")

	requestMethod, requestPath, requestVersion := parsedRequestLine(lines[0])

	if requestVersion != "HTTP/1.1" {
		// Don't know how to handle.
		return
	}

	if requestPath == "/" {
		// Implicit file to serve.
		requestPath = "/index.html"
	}

	if requestMethod == "GET" {
		serveStaticFile(conn, requestPath)
	}
}

func serveStaticFile(conn net.Conn, requestPath string) {
	body, err := os.ReadFile(path.Join("public", requestPath))
	if err == os.ErrNotExist {
		conn.Write([]byte("HTTP/1.1 404 Not Found\r\n"))
		conn.Write([]byte("Connection: close\r\n"))
		return
	}
	if err != nil {
		conn.Write([]byte("HTTP/1.1 500 Internal Server Error\r\n"))
		conn.Write([]byte("Connection: close\r\n"))
		return
	}

	conn.Write([]byte("HTTP/1.1 200 OK\r\n"))
	conn.Write([]byte("Connection: close\r\n"))
	conn.Write([]byte(fmt.Sprintf("Connection-Length: %d\r\n", len(body))))
	conn.Write([]byte("\r\n"))
	conn.Write(body)
}

func parsedRequestLine(line string) (string, string, string) {
	parts := strings.Split(line, " ")
	return parts[0], parts[1], parts[2]
}
