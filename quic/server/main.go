package main

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"log"
	"math/big"

	"github.com/lucas-clemente/quic-go"
)

func main() {
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{generateTLSCertificate()},
		NextProtos:   []string{"example_v1"},
	}

	listener, _ := quic.ListenAddr("localhost:3333", tlsConfig, nil)
	log.Printf("Listening on %s", listener.Addr())

	for {
		conn, _ := listener.Accept(context.Background())
		handleConnection(conn)
		// leaking connections to simplify example.
	}
}

func handleConnection(conn quic.Connection) {
	stream, _ := conn.AcceptStream(context.Background())
	defer stream.Close()

	buf := make([]byte, 1024)
	stream.Read(buf)
	log.Printf("Received from the request: %s", string(buf))

	response := "Echo " + string(buf)
	log.Printf("Sending back: %s", response)
	stream.Write([]byte(response))
}

func generateTLSCertificate() tls.Certificate {
	key, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		panic(err)
	}
	template := x509.Certificate{SerialNumber: big.NewInt(1)}
	certDER, err := x509.CreateCertificate(rand.Reader, &template, &template, &key.PublicKey, key)
	if err != nil {
		panic(err)
	}
	keyPEM := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(key)})
	certPEM := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: certDER})

	tlsCert, err := tls.X509KeyPair(certPEM, keyPEM)
	if err != nil {
		panic(err)
	}
	return tlsCert
}
