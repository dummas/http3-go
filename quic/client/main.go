package main

import (
	"context"
	"crypto/tls"
	"log"

	"github.com/lucas-clemente/quic-go"
)

func main() {
	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
		NextProtos:         []string{"example_v1"},
	}

	conn, _ := quic.DialAddr("localhost:3333", tlsConfig, nil)
	log.Printf("Connected to %s", conn.RemoteAddr())

	stream, _ := conn.OpenStreamSync(context.Background())
	defer stream.Close()

	message := "Hello"
	log.Printf("Sending to server: %s", message)
	stream.Write([]byte(message))

	buf := make([]byte, 1024)
	stream.Read(buf)
	log.Printf("Received from server: %s", string(buf))
}
