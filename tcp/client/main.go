package main

import (
	"log"
	"net"
)

func main() {
	conn, _ := net.Dial("tcp", "localhost:3333")
	defer conn.Close()
	log.Printf("Connected to %s", conn.RemoteAddr())

	message := "Hello"
	log.Printf("Sending to server: %s", message)
	conn.Write([]byte(message))

	buf := make([]byte, 1024)
	conn.Read(buf)
	log.Printf("Response from server: %s", string(buf))
}
