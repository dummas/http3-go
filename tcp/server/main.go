package main

import (
	"log"
	"net"
)

func main() {
	listener, _ := net.Listen("tcp", "localhost:3333")
	defer listener.Close()
	log.Printf("Listening on %s", listener.Addr())

	for {
		conn, _ := listener.Accept()

		buf := make([]byte, 1024)
		conn.Read(buf)
		log.Printf("Received from the request: %s", string(buf))

		response := "Echo " + string(buf)
		log.Printf("Sending back: %s", response)
		conn.Write([]byte(response))

		conn.Close()
	}
}
